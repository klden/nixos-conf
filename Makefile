update:
	nix flake update --commit-lock-file

clean:
	sudo nix-collect-garbage -d --delete-older-than 5d
	sudo nix-store --optimise
	sudo nix-env -p /nix/var/nix/profiles/system --delete-generations 5d
	sudo /run/current-system/bin/switch-to-configuration boot
	sudo nix-store --verify --repair
	# rm -rf ~/.cache
	# /home/klden/.cache/nix/
	# sudo du -h --max-depth=4 / | sort -rh | head -n 20

