{ config, lib, inputs, pkgs, modulesPath, flake, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usb_storage" "usbhid" "sd_mod" ];
  boot.initrd.kernelModules = [ "dm-snapshot" "wl" ];
  boot.kernelModules = [ "kvm-amd" "wl" ];
  boot.extraModulePackages = [ config.boot.kernelPackages.broadcom_sta ];
  boot.initrd.luks.devices.root = {
    device = "/dev/nvme0n1p2";
    preLVM = true;
  };

  fileSystems."/" =
    {
      device = "/dev/disk/by-uuid/a417e131-afad-404f-a5fb-2eb14777bfa5";
      fsType = "xfs";
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/B2D9-2C26";
      fsType = "vfat";
    };

  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

  swapDevices =
    [{ device = "/dev/disk/by-uuid/99562496-cdc0-49fe-815f-0bd057449b11"; }];

  nix = {
    package = pkgs.nixVersions.git;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    settings = {
      trusted-users = [ "root" flake.config.people.myself ];
    };
  };

  networking.hostName = "desktop";

  programs.zsh.enable = true;
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    extraUsers.${flake.config.people.myself} = {
      extraGroups = [ "wheel" "video" "audio" "disk" ];
      group = "users";
      isNormalUser = true;
      uid = 1000;
      shell = pkgs.zsh;
    };

    users.${flake.config.people.myself} = {
      subUidRanges = [{ startUid = 10000; count = 65536; }];
      subGidRanges = [{ startGid = 10000; count = 65536; }];
    };
    users.root.initialHashedPassword = "";
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  boot = {
    loader = {
      timeout = 10;
      efi.canTouchEfiVariables = true;
      grub = {
        enable = true;
        devices = [ "nodev" ];
        efiSupport = true;
        useOSProber = true;
        configurationLimit = 15;
      };
    };

    tmp.cleanOnBoot = true;
  };

  i18n = {
    extraLocaleSettings = {
      LC_MESSAGES = "en_US.UTF-8";
      LC_TIME = "en_US.UTF-8";
    };
  };

  services = {
    displayManager = {
      autoLogin = {
        enable = true;
        user = "${flake.config.people.myself}";
      };
    };

    fwupd.enable = true;
    pcscd.enable = true;

    udev = {
      packages = with pkgs; [
        yubikey-personalization
        android-udev-rules
      ];
    };

    # Enable CUPS to print documents.
    #printing.enable = true;

    xserver = {
      videoDrivers = [ "nouveau" ];

      layout = "us";
      xkbVariant = "altgr-intl";
    };

    resolved = {
      enable = true;
      # https://flaviutamas.com/2020/fix-slow-lan-name-resolution
      llmnr = "false";
      extraConfig = "MulticastDNS=true\nDNSSEC=false";
      fallbackDns = [
        "1.1.1.1"
      ];
    };
  };

  networking = {
    wireless.enable = false; # disable wpa_sup
    wireless.iwd.enable = true;
    useNetworkd = true;
    # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
    # (the default) this is the recommended approach. When using systemd-networkd it's
    # still possible to use this option, but it's recommended to use it in conjunction
    # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
    useDHCP = lib.mkDefault true;
  };

  systemd = {
    network = {
      enable = true;
    };
  };

  time.timeZone = "America/Montreal";

  system.stateVersion = "22.11";
}

