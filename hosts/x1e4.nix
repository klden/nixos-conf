{ config, lib, inputs, pkgs, modulesPath, flake, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "thunderbolt" "nvme" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
  boot.initrd.kernelModules = [
    "dm-snapshot"
    "v4l2loopback" # Virtual Camera

  ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [
    config.boot.kernelPackages.v4l2loopback.out
  ];

  # Set initial kernel module settings
  boot.extraModprobeConfig = ''
    # exclusive_caps: Skype, Zoom, Teams etc. will only show device when actually streaming
    # card_label: Name of virtual camera, how it'll show up in Skype, Zoom, Teams
    # https://github.com/umlaeute/v4l2loopback
    options v4l2loopback exclusive_caps=1 card_label="Virtual Camera"
  '';

  fileSystems."/" =
    {
      device = "/dev/disk/by-uuid/2c797691-2c72-4ceb-b5b5-fcabe9d2ea66";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/4D1A-FC55";
      fsType = "vfat";
    };

  swapDevices =
    [{ device = "/dev/disk/by-uuid/8ee97b47-5fa6-4277-a915-11ddda538912"; }];

  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

  boot.initrd.luks.devices.root = {
    device = "/dev/nvme0n1p2";
    preLVM = true;
  };

  nix = {
    package = pkgs.nixVersions.git;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    settings = {
      trusted-users = [ "root" flake.config.people.myself ];
    };
  };

  networking.hostName = "x1e4";

  hardware = {
    cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

    graphics = {
      enable = true;
      enable32Bit = true;
    };

    bluetooth = {
      enable = true;
      settings = {
        General = {
          Enable = "Source,Sink,Media,Socket";
        };
      };
    };
  };

  # https://discourse.nixos.org/t/protobuf-cant-be-run/13568/4
  systemd.tmpfiles.rules = [
    "L+ /lib64/ld-linux-x86-64.so.2 - - - - ${pkgs.glibc}/lib64/ld-linux-x86-64.so.2"
  ];

  services.openvpn.servers = {
    ar = {
      config = "config /root/openvpn/ar.ovpn";
      up = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      down = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      autoStart = false;
    };
    ca = {
      config = "config /root/openvpn/ca.ovpn";
      up = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      down = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      autoStart = false;
    };
    us = {
      config = "config /root/openvpn/us.ovpn";
      up = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      down = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      autoStart = false;
    };
    vn = {
      config = "config /root/openvpn/vn.ovpn";
      up = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      down = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      autoStart = false;
    };
    adf = {
      config = "config /root/openvpn/adf.ovpn";
      up = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      down = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      autoStart = false;
    };
    cste = {
      config = ''
        config /root/openvpn/cste_tcp.ovpn

        script-security 2
        up ${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved
        up-restart
        down ${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved
        down-pre
      '';
      autoStart = false;
    };
    cste2 = {
      config = ''
        config /root/openvpn/cste2.ovpn

        script-security 2
        up ${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved
        up-restart
        down ${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved
        down-pre
      '';
      autoStart = false;
    };
    nsec = {
      config = "config /root/openvpn/nsec.ovpn";
      up = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      down = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
      autoStart = false;
    };
  };

  systemd = {
    services = {
      systemd-networkd-wait-online.enable = true;
    };
    network = {
      enable = true;
      wait-online.anyInterface = true;
    };
  };

  networking = {
    # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
    # (the default) this is the recommended approach. When using systemd-networkd it's
    # still possible to use this option, but it's recommended to use it in conjunction
    # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
    useDHCP = lib.mkForce false;
    interfaces = {
      wlan0.useDHCP = true;
      enp0s20f0u6.useDHCP = true;
      enp0s20f0u1.useDHCP = true;
      enp0s13f0u2u2u4.useDHCP = true;
    };
    #  wg0 = {
    #    address = [ "10.8.0.22/24" ];
    #    dns = ["1.1.1.1" "1.0.0.1"];
    #    privateKeyFile = "/root/wireguard/privkey.conf";
    #    peers = [
    #      {
    #        publicKey = "L4EvvRwIowjGQqsF/dZZkHiiYY2GHtqvLUIibwF1QhU=";
    #        presharedKeyFile = "/root/wireguard/presharedkey.conf";
    #        allowedIPs = [ "0.0.0.0/0" "::/0" ];
    #        endpoint = "wg.vpn1.holidayctf.ca:51820";
    #        persistentKeepalive = 25;
    #      }
    #    ];
    #  };
    #};
    #wg-quick.interfaces = {
    #  wg0 = {
    #    address = [ "2602:fc62:ef:2011::103/64" ];
    #    dns = ["1.1.1.1" "1.0.0.1" "2602:fc62:ef:2::1"];
    #    privateKeyFile = "/root/wireguard/northsec/privkey.conf";
    #    peers = [
    #      {
    #        publicKey = "GYK3Oh1Ff1i3wRJi7BPGDDbgOeE1rr5OcQO9ljOTZEI=";
    #        allowedIPs = [ "2602:fc62:ef::/48" "9000::/16" ];
    #        endpoint = "vpn01.ctf.nsec.io:51011";
    #        persistentKeepalive = 25;
    #      }
    #    ];
    #  };
    #};
  };

  programs.zsh.enable = true;
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    extraUsers.${flake.config.people.myself} = {
      extraGroups = [ "wheel" "video" "audio" "disk" ];
      group = "users";
      isNormalUser = true;
      uid = 1000;
      shell = pkgs.zsh;
    };

    users.${flake.config.people.myself} = {
      subUidRanges = [{ startUid = 10000; count = 65536; }];
      subGidRanges = [{ startGid = 10000; count = 65536; }];
    };
    users.root.initialHashedPassword = "";
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  boot = {
    loader = {
      timeout = 10;
      efi.canTouchEfiVariables = true;
      systemd-boot = {
        enable = true;
        configurationLimit = 15;
      };
    };

    tmp.cleanOnBoot = true;
  };

  i18n = {
    extraLocaleSettings = {
      LC_MESSAGES = "en_US.UTF-8";
      LC_TIME = "en_US.UTF-8";
    };
  };

  services = {
    displayManager = {
      autoLogin = {
        enable = true;
        user = "${flake.config.people.myself}";
      };
    };

    libinput.enable = true;
    fwupd.enable = true;

    # clam shell mode
    logind.lidSwitchExternalPower = "ignore";

    pcscd.enable = true;

    udev = {
      packages = with pkgs; [
        yubikey-personalization
        android-udev-rules
      ];
    };

    # Enable CUPS to print documents.
    #printing.enable = true;

    xserver = {
      xkb.layout = "us";
      xkb.variant = "altgr-intl";
    };

    resolved = {
      enable = true;
      # https://flaviutamas.com/2020/fix-slow-lan-name-resolution
      llmnr = "false";
      extraConfig = "MulticastDNS=true\nDNSSEC=false";
    };
  };

  time.timeZone = "America/Montreal";

  system.stateVersion = "22.11";
}

