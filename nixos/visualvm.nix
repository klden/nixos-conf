{ pkgs, ... }:

{
  environment = {
    systemPackages = (with pkgs; [
      visualvm
    ]);
  };
}

