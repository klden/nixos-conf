{ self, inputs, config, ... }:
let
  overlayModule = {
    nixpkgs.overlays = [
      (inputs.nixpkgs-wayland.overlay)
    ];
  };
  mkHomeModule = name: extraModules: {
    users.users.${name}.isNormalUser = true;
    home-manager.users.${name} = {
      imports = [
        self.homeModules.common-linux
      ] ++ extraModules;
    };
  };
in
{
  # Configuration common to all Linux systems
  flake = {
    nixosModules = {
      myself = mkHomeModule config.people.myself [
        ../home/bibata-cursors.nix
      ];

      default.imports = [
        overlayModule
        self.nixosModules.home-manager
        self.nixosModules.myself
        ./caches
        #./monoid.nix
        ./light.nix
        ./adb.nix
        ./docker.nix
        ./signal-desktop.nix
        ./java.nix
        ./retext.nix
        ./gnupg.nix
        ./libvirt.nix
        ./libreoffice.nix
        ./vlc.nix
        ./thunar.nix
        ./xournalpp.nix
        ./okular.nix
        ./ark.nix
        ./yubikey.nix
        ./kile-texlive.nix
        ./imv.nix
        ./pdftk.nix
        ./wireguard-tools.nix
      ];
    };
  };
}

