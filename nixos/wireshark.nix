{ pkgs, config, flake, ... }:
{
  users.users.${flake.config.people.myself} = {
    extraGroups = [ "wireshark" ];
  };

  programs.wireshark.enable = true;

  environment = {
    systemPackages = (with pkgs; [
      wireshark
    ]);
  };
}

