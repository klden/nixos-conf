{ pkgs, ... }: {
  services.tlp = {
    enable = true;
    settings = {
      RUNTIME_PM_ON_AC = "auto";
      # Operation mode when no power supply can be detected: AC, BAT.
      TLP_DEFAULT_MODE = "BAT";
      # Operation mode select: 0=depend on power source, 1=always use TLP_DEFAULT_MODE
      TLP_PERSISTENT_DEFAULT = 1;
      # Do not suspend USB devices
      USB_AUTOSUSPEND = 0;
      # Set battery charge thresholds for main/internal battery BAT0
      START_CHARGE_THRESH_BAT0 = 70;
      STOP_CHARGE_THRESH_BAT0 = 95;
      # Restore configured charge thresholds when AC is unplugged
      RESTORE_THRESHOLDS_ON_BAT = 1;
    };
  };
}

