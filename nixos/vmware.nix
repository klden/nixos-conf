{ pkgs, config, flake, ... }:
{
  virtualisation.vmware.guest.enable = true;
  virtualisation.vmware.host.enable = true;
}

