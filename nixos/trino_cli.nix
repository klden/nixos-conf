{ pkgs, ... }:
{
  environment = {
    systemPackages = (with pkgs; [
      trino-cli
    ]);
  };
}

