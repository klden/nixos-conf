{ pkgs, ... }: {
  imports = [
    ./nixpkgs.nix
    ./nixpkgs-wayland.nix
    ./hyprland.nix
  ];
}

