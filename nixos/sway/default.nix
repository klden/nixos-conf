{ config, flake, pkgs, lib, ... }:
{
  services.getty.autologinUser = flake.config.people.myself;

  # enabling login display managers slows down graphical session for some reasons...
  services.greetd = {
    enable = false;
    restart = false;
    settings = {
      default_session = {
        command = "${lib.makeBinPath [pkgs.greetd.tuigreet] }/tuigreet --time --remember --asterisks --cmd sway";
        user = "greeter";
      };
      initial_session = {
        command = "sway";
        user = flake.config.people.myself;
      };
    };
  };

  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true; # so that gtk works properly
    extraOptions = [
      "--unsupported-gpu"
    ];
    extraPackages = with pkgs; [
      swaylock-effects
      swayidle
      xorg.xlsclients
      wl-clipboard
      mako # notification daemon
      foot # terminal
      rofi-wayland
      xwayland # for legacy apps
      waybar
      libappindicator # status bar
      gammastep # redshift for wayland
      swappy
      grim
      slurp # screenshot
      flashfocus
      google-chrome
      polkit_gnome
      helvum
      shikane # wrandl for sway
      easyeffects # adjust volume in pipewire
      wl-clipboard
      wf-recorder # screen recorder
    ];
    extraSessionCommands = ''
      export _JAVA_AWT_WM_NONREPARENTING=1
      export MOZ_ENABLE_WAYLAND=1
      export MOZ_DBUS_REMOTE=1
      export MOZ_USE_XINPUT2=1
      export QT_QPA_PLATFORM=wayland
      export QT_QPA_PLATFORMTHEME=qt5ct
      export SDL_VIDEODRIVER=wayland
      export XDG_CURRENT_DESKTOP=sway
      export XDG_SESSION_TYPE=wayland
    '';
    # https://bbs.archlinux.org/viewtopic.php?id=251351
    #export GDK_BACKEND=wayland
  };

  environment = {
    sessionVariables.NIXOS_OZONE_WL = "1";
    loginShellInit = ''
            if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
      	exec sway
            fi
    '';
    etc = {
      # Put config files in /etc. Note that you also can put these in ~/.config, but then you can't manage them with NixOS anymore!
      "sway/config".source = ./config;
      "xdg/waybar/config".source = ./waybar/config;
      "xdg/waybar/style.css".source = ./waybar/style.css;
      "xdg/swappy/config".source = ./swappy/config;
      "xdg/mako/config".source = ./mako/config;
      "xdg/shikane/config.toml".source = ./shikane/config.toml;
      "xdg/electron-flags.conf".source = ./electron/electron-flags.conf;
    };
    # polkit https://nixos.wiki/wiki/Sway
    pathsToLink = [ "/libexec" ];
  };

  fonts.packages = with pkgs; [
    font-awesome # for waybar 
  ];

  # required for firefox on wayland
  # https://wiki.archlinux.org/title/PipeWire#WebRTC_screen_sharing
  xdg = {
    portal = {
      enable = true;
      xdgOpenUsePortal = true;
      wlr = {
        enable = true;
        settings = {
          screencast = {
            chooser_type = "simple";
            chooser_cmd = "${pkgs.slurp}/bin/slurp -f %o -or";
          };
        };
      };
      extraPortals = with pkgs; [
        xdg-desktop-portal-wlr
        xdg-desktop-portal-gtk
      ];
    };
  };

  sound.enable = false;

  # rtkit is optional but recommended
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    wireplumber.enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };
}

