{ pkgs, ... }:

{
  environment = {
    systemPackages = (with pkgs; [
      eclipse-mat
    ]);
  };
}

