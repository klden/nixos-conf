{ pkgs, ... }: {
  networking = {
    wireless.enable = false;
    wireless.iwd.enable = true;
    useNetworkd = true;
  };

  services = {
    connman.enable = true;
    connman.wifi.backend = "iwd";
  };

  environment = {
    systemPackages = (with pkgs; [
      cmst
    ]);
  };
}

