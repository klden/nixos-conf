{ pkgs, ... }:
{
  hardware.opengl.extraPackages = with pkgs; [ libva ];
  programs.steam.enable = true;
}

