{ pkgs, config, docker, flake, ... }:
{
  virtualisation.docker = {
    enable = true;
    daemon.settings = {
      experimental = true;
    };
  };

  users.users.${flake.config.people.myself} = {
    extraGroups = [ "lxd" "docker" ];
  };

  environment = {
    systemPackages = (with pkgs; [
      docker-compose
    ]);
  };
}

