#!/usr/bin/env bash

# Get the current time with timezone
TIME=$(date +"%H:%M %Z")

# Output the time
echo "{\"text\": \"$TIME\", \"tooltip\": \"$TIME\"}"

