{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell rec {
  buildInputs = [
    pkgs.gobject-introspection
    (pkgs.python312.withPackages (ps: with ps; [
      pygobject3
    ]))
    pkgs.playerctl
  ];

  shellHook = ''
    export LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
    export LD_LIBRARY_PATH="${pkgs.stdenv.cc.cc.lib}/lib:$LD_LIBRARY_PATH"
  '';
}
