{ config, flake, pkgs, lib, ... }:
{
  services.getty.autologinUser = flake.config.people.myself;

  # enabling login display managers slows down graphical session for some reasons...
  services.greetd = {
    enable = false;
    restart = false;
    settings = {
      default_session = {
        command = "${lib.makeBinPath [pkgs.greetd.tuigreet] }/tuigreet --time --remember --asterisks --cmd Hyprland --config /etc/hyprland/config";
        user = "greeter";
      };
      initial_session = {
        command = "Hyprland --config /etc/hyprland/config";
        user = flake.config.people.myself;
      };
    };
  };

  environment = {
    systemPackages = (with pkgs; [
      playerctl
      iwgtk
      foot
      xorg.xlsclients
      wl-clipboard
      mako # notification daemon
      rofi-wayland
      libappindicator # status bar
      gammastep # redshift for wayland
      swappy
      obs-studio # screen sharing
      grim
      slurp # screenshot
      firefox-devedition-bin
      chromium
      polkit-kde-agent
      helvum
      shikane # wrandl for sway
      easyeffects # adjust volume in pipewire
      wl-clipboard
      wf-recorder # screen recorder
      hyprlock
      hypridle
      hyprland-qtutils
      #hyprlandPlugins.hy3
      hyprpaper
      wlogout
    ]);
  };

  programs = {
    hyprland = {
      enable = true;
      xwayland.enable = true; # for legacy apps
    };
    waybar = {
      enable = true;
    };
  };

  environment = {
    loginShellInit = ''
            if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
      	exec Hyprland --config /etc/hyprland/config
            fi
    '';
    etc = {
      # Put config files in /etc. Note that you also can put these in ~/.config, but then you can't manage them with NixOS anymore!
      "hyprland/config".source = ./config;
      "xdg/hypr/hyprlock.conf".source = ./hyprlock/hyprlock.conf;
      "xdg/hypr/hypridle.conf".source = ./hypridle/hypridle.conf;
      "xdg/hypr/hyprpaper.conf".source = ./hyprpaper/hyprpaper.conf;
      "xdg/hypr/hyprpaper/images".source = ./hyprpaper/images;
      "xdg/wlogout/layout".source = ./wlogout/layout;
      "xdg/wlogout/style.css".source = ./wlogout/style.css;
      "xdg/wlogout/icons".source = ./wlogout/icons;
      "xdg/waybar".source = ./waybar;
      "xdg/foot/foot.ini".source = ./foot/foot.ini;
      "xdg/swappy/config".source = ./swappy/config;
      "xdg/mako/config".source = ./mako/config;
      "xdg/shikane/config.toml".source = ./shikane/config.toml;
      "xdg/electron-flags.conf".source = ./electron/electron-flags.conf;
    };
    # polkit https://nixos.wiki/wiki/Sway
    pathsToLink = [ "/libexec" ];
  };

  fonts.packages = with pkgs; [
    font-awesome # for waybar 
  ];

  # required for firefox on wayland
  # https://wiki.archlinux.org/title/PipeWire#WebRTC_screen_sharing
  xdg = {
    portal = {
      enable = true;
      xdgOpenUsePortal = true;
      wlr = {
        enable = lib.mkForce true;
        settings = {
          screencast = {
            chooser_type = "simple";
            chooser_cmd = "${pkgs.slurp}/bin/slurp -f %o -or";
          };
        };
      };
      extraPortals = with pkgs; [
        xdg-desktop-portal-hyprland
        xdg-desktop-portal-gtk
      ];
    };
  };

  # Set default applications for MIME types
  xdg.mime.defaultApplications = {
    "inode/directory" = "pcmanfm.desktop";
    "x-scheme-handler/http" = "firefox-developer-edition.desktop";
    "x-scheme-handler/https" = "firefox-developer-edition.desktop";
  };

  # rtkit is optional but recommended
  security.rtkit.enable = true;
  security.polkit.enable = true;

  services.pipewire = {
    enable = true;
    wireplumber.enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };
}

