{ stdenv, fetchFromGitHub, sbt, scala_3, makeWrapper, ... }:

stdenv.mkDerivation rec {
  name = "tfr";
  src = fetchFromGitHub {
    owner = "spotify";
    repo = "tfreader";
    rev = "v0.1.2";
    sha256 = "sha256-BJ+Dvrrzfa0PQf0euIfPODYBcqTe5wwAv2JGNhvGbrM="; # Replace this with the actual sha256 hash
  };

  nativeBuildInputs = [
    sbt
  ];

  buildInputs = [
    sbt
    makeWrapper
  ];

  buildPhase = ''
    cd ${src}
    sbt "project cli" assembly
  '';

  installPhase = ''
    mkdir -p $out/bin
    mkdir -p $out/share/java
    cp modules/cli/target/scala-*/tfr-cli-*.jar $out/share/java

    makeWrapper ${scala_3}/bin/scala $out/bin/${name} \
      --add-flags "-cp \"$out/share/java/*\" --help" 
  '';
}

