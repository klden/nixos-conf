{ pkgs, lib, ... }:

{
  environment = {
    systemPackages = (with pkgs; [
      insomnia
    ]);
  };
}

