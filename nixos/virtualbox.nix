{ pkgs, config, ... }:
{
  virtualisation.virtualbox.host = {
    enable = true;
    enableExtensionPack = true;
  };
  virtualisation.virtualbox.guest.enable = true;
  users.extraGroups.vboxusers.members = [ flake.config.people.myself ];
}

