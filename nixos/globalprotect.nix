{ pkgs, ... }:

{
  # https://github.com/yuezk/GlobalProtect-openconnect/issues/69#issuecomment-988957394
  environment = {
    systemPackages = (with pkgs; [
      gpauth
      gpclient
      glib-networking
    ]);
  };
}

