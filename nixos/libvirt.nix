{ pkgs, config, flake, ... }:
{
  virtualisation.libvirtd.enable = true;
  users.extraGroups.libvirtd.members = [ flake.config.people.myself ];
  users.extraGroups.qemu-libvirtd.members = [ flake.config.people.myself ];
  environment.systemPackages = with pkgs; [
    virt-manager
  ];
}

