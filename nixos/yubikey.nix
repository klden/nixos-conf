{ pkgs, ... }:

{
  environment = {
    systemPackages = (with pkgs; [
      yubikey-personalization
      yubikey-manager-qt
    ]);
  };
}

