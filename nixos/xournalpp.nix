{ pkgs, ... }:

{
  environment = {
    systemPackages = (with pkgs; [
      xournalpp
      adwaita-icon-theme
      shared-mime-info
    ]);
  };

  # https://github.com/NixOS/nixpkgs/issues/163107#issuecomment-1100569484
  environment.pathsToLink = [
    "/share/icons"
    "/share/mime"
  ];
}

