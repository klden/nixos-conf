{ pkgs, ... }:
{
  environment = {
    systemPackages = (with pkgs; [
      (jetbrains.plugins.addPlugins jetbrains.rust-rover [ "github-copilot" "ideavim" ])
      nodejs-slim
    ]);
  };
}

