{ pkgs, ... }:

{
  environment = {
    systemPackages = (with pkgs; [
      jetbrains.datagrip
      nodejs-slim
    ]);
  };
}
