{ pkgs, ... }:
{
  environment = {
    systemPackages = (with pkgs; [
      (jetbrains.plugins.addPlugins jetbrains.pycharm-professional [ "github-copilot" "ideavim" ])
      nodejs-slim
    ]);
  };
}

