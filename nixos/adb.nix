{ pkgs, config, flake, ... }:
{
  programs.adb = {
    enable = true;
  };
  users.extraUsers.${flake.config.people.myself}.extraGroups = [ "adbusers" ];
}

