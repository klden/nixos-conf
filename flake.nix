{
  description = "klden's NixOS config";

  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-wayland.url = "github:nix-community/nixpkgs-wayland";
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nixpkgs-wayland.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ self, home-manager, nixpkgs, ... }:
    inputs.flake-parts.lib.mkFlake { inherit (inputs) self; } {
      systems = [ "x86_64-linux" ];
      imports = [
        ./lib.nix
        ./config.nix
        ./home
        ./nixos
      ];

      people = {
        myself = "klden";
        users = {
          klden = {
            name = "Kenzyme L";
            email = "le.kenzyme@pm.me";
          };
        };
      };

      flake = {
        # Configurations for Linux (NixOS) systems
        nixosConfigurations = {
          # My Linux development computers 
          x1e4 = self.lib.mkLinuxSystem {
            nixpkgs.config.allowUnfree = true;
            nixpkgs.config.permittedInsecurePackages = [
              "electron-29.4.6"
            ];
            imports = [
              self.nixosModules.default # Defined in nixos/default.nix
              inputs.nixos-hardware.nixosModules.lenovo-thinkpad-x1-extreme-gen4
              ./hosts/x1e4.nix
              ./nixos/connman-iwd.nix
              ./nixos/hyprland
              ./nixos/tlp.nix
              ./nixos/openvpn.nix
              ./nixos/globalprotect.nix
              ./nixos/jetbrains/enterprise/intellij.nix
              ./nixos/jetbrains/enterprise/pycharm.nix
              #./nixos/jetbrains/enterprise/rust-rover.nix
              #./nixos/jetbrains/enterprise/webstorm.nix
              ./nixos/visualvm.nix
              ./nixos/eclipse-mat.nix
              ./nixos/blueman.nix
              ./nixos/spotify.nix
              ./nixos/protonvpn.nix
              ./nixos/proton-pass.nix
              ./nixos/postman.nix
              #./nixos/vmware.nix
              ./nixos/trino_cli.nix
              ./nixos/slack.nix
              #./nixos/teams-for-linux.nix
            ];
          };
          desktop = self.lib.mkLinuxSystem {
            nixpkgs.config.allowUnfree = true;
            imports = [
              self.nixosModules.default # Defined in nixos/default.nix
              ./hosts/desktop.nix
              ./nixos/hyprland
              ./nixos/jetbrains/community/intellij.nix
              ./nixos/wireshark.nix
              ./nixos/broadcom_sta.nix
              ./nixos/vmware.nix
              ./nixos/deluge.nix
              ./nixos/spotify.nix
              ./nixos/proton-pass.nix
            ];
          };
        };
      };

      perSystem = { pkgs, config, ... }: {
        formatter = pkgs.nixpkgs-fmt;
        apps.default = config.apps.activate;
      };
    };
}

