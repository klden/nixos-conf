{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell rec {
  buildInputs = [
    pkgs.pkg-config
    pkgs.openssl
    pkgs.rustup
    pkgs.zlib
  ];

  shellHook = ''
    export LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
    export LD_LIBRARY_PATH="${pkgs.stdenv.cc.cc.lib}/lib:$LD_LIBRARY_PATH"
    export LD_LIBRARY_PATH="${pkgs.zlib}/lib:$LD_LIBRARY_PATH"
  '';
}
