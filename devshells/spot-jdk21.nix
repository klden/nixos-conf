{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    jdk21
    (sbt.overrideAttrs (finalAttrs: previousAttrs: { postPatch = ''''; }))
    maven
    scala
    ammonite

    python312Full
    python312Packages.pip
    python312Packages.srvlookup
    python312Packages.venvShellHook
    python312Packages.requests

    grpcurl
    (google-cloud-sdk.withExtraComponents ([
      google-cloud-sdk.components.gke-gcloud-auth-plugin
      google-cloud-sdk.components.kubectl
      google-cloud-sdk.components.cbt
    ]))
    kubectx
    (kubetail.overrideAttrs (finalAttrs: previousAttrs: {
      installPhase = ''
        		install -Dm755 kubetail "$out/bin/kubetail"
        		#wrapProgram $out/bin/kubetail --prefix PATH : ${lib.makeBinPath [ kubectl ]}
        		installShellCompletion completion/kubetail.{bash,fish,zsh}
        	      '';
    }))

    nixpkgs-fmt
  ];
  venvDir = "/home/klden/.venv";
  postVenvCreation = ''
    export PIP_EXTRA_INDEX_URL="https://pypi.spotify.net/simple/"
    pip install spotify-quick-styx
  '';
  postShellHook = ''
    export HISTSIZE=20000
    export HISTFILESIZE=20000
    export LD_LIBRARY_PATH="${pkgs.stdenv.cc.cc.lib}/lib"
  '';
}

