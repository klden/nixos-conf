{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell rec {
  buildInputs = [
    pkgs.gcc
    pkgs.stdenv.cc
    pkgs.stdenv.cc.cc
    pkgs.ltrace
    pkgs.python312
    pkgs.python312Packages.fastecdsa
    pkgs.python312Packages.requests
    pkgs.python312Packages.pip
    pkgs.zip
    pkgs.unzip
    pkgs.zlib

    pkgs.openssl
    # forensics
    pkgs.testdisk
    pkgs.foremost
    pkgs.ghidra

    # exploit
    pkgs.metasploit
    pkgs.thc-hydra
  ];

  shellHook = ''
    export LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
    export LD_LIBRARY_PATH="${pkgs.stdenv.cc.cc.lib}/lib:$LD_LIBRARY_PATH"
  '';
}
