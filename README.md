Inspired by [srid's config](https://github.com/srid/nixos-config)

This repository contains the Nix / NixOS configuration for all of my systems. Start from `flakes.nix` (see [Flakes](https://nixos.wiki/wiki/Flakes)) if you are looking for NixOS configuration.

- `flake.nix`: Install things I need natively on NixOS desktop & laptop computers

Installation of NixOS with LUKS

https://gist.github.com/martijnvermaat/76f2e24d0239470dd71050358b4d5134

