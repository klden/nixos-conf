{ pkgs, ... }:
{
  programs = {
    zsh = {
      enable = true;
      autosuggestion.enable = true;
      syntaxHighlighting.enable = true;
      enableCompletion = true;
      defaultKeymap = "emacs";
      shellAliases = {
        cat = "bat";
        #grep = "rga";
        nix = "noglob nix"; # https://github.com/NixOS/nix/issues/4686
      };
      sessionVariables = {
        EDITOR = "vim";
        VISUAL = "vim";
        BROWSER = "firefox";
        GTK_USE_PORTAL = "1";
      };
      initExtra = ''
        bindkey "^[[1;5C" forward-word
        bindkey "^[[1;5D" backward-word

        if [[ $DISPLAY ]]; then
          # If not running interactively, don't do anything
          [[ $- != *i* ]] && return
       
          if [[ -z "$TMUX" ]] ;then
            ID="$( tmux ls | grep -vm1 attached | cut -d: -f1 )" # get the id of a deattached session
              # prevent starting TMUX in Jetbrain's Terminal
              if [[ "$TERMINAL_EMULATOR" == "JetBrains-JediTerm" ]] ; then
                  ZSH_TMUX_AUTOSTART=false;
            elif [[ -z "$ID" ]] ;then # if not available create a new one
            	tmux new-session
            else
            	tmux attach-session -t "$ID" # if available attach to it
            fi
          fi
        fi
        # make sec ENVS available to all my terms
        source $HOME/.zshenv.sec 2>/dev/null || true
      '';
      prezto = {
        enable = true;
        tmux.autoStartLocal = true;
        prompt.theme = "walters";
      };
    };
  };
}

