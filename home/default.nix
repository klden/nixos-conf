{ self, inputs, config, ... }:
{
  flake = {
    homeModules = {
      common = {
        home.stateVersion = "24.11";
        fonts.fontconfig.enable = true;
        imports = [
          ./tmux.nix
          ./git.nix
          ./vim.nix
          ./zsh.nix
          ./terminal.nix
        ];
      };
      common-linux = {
        imports = [
          self.homeModules.common
        ];
      };
    };
  };
}

