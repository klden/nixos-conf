{ pkgs, ... }:
{
  programs = {
    jq.enable = true;
    ssh.enable = true;
    bat.enable = true;
    lsd = {
      enable = true;
      enableAliases = true;
    };
  };

  home.packages = with pkgs; [
    curl
    tree
    git
    wget
    which
    htop
    file
    ntfs3g
    woeusb
    gnumake
    gcc
    binutils
    bc
    bind
    usbutils
    dmidecode
    p7zip
    #ripgrep-all
    fd
    sd
    procs
    bandwhich
    lsof
    pciutils
    traceroute
    nmap
    fuse
  ];
}

