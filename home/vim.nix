{ pkgs, ... }:
{
  programs = {
    vim = {
      enable = true;
      extraConfig = ''
                set hlsearch
                set autoindent
                set smartindent
                set ruler
                set shiftwidth=4
                set softtabstop=4
                set expandtab
                set clipboard+=unnamedplus  " use the clipboards of vim and win
                set paste               " Paste from a windows or from vim
                set go+=a               " Visual selection automatically copied to the clipboardet nostartofline
                set nu
                set ignorecase
                set smartcase
                set incsearch
        	set backspace=indent,eol,start
                color desert
                syntax on
                xnoremap <silent> <C-c> :w !wl-copy<CR><CR>
      '';
    };
  };
}

